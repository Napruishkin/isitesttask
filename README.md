# Simple Chat Application

This is a simple chat application developed using Django and Django Rest Framework.

## Installation

1. Clone the repository:
   ```
   git clone https://gitlab.com/Napruishkin/isitesttask
   cd isitesttask
   ```

2. Install dependencies:
   ```
   pip install -r requirements.txt
   ```

3. Apply migrations:
   ```
   python manage.py migrate
   ```

4. Run the server:
   ```
   python manage.py runserver
   ```

## Usage

### Endpoints:

- **Create Thread:** `POST /api/thread/`
  - Creates a new thread. If a thread with the same participants already exists, it returns the existing thread.
  
- **Delete Thread:** `DELETE /api/thread/<thread_id>/`
  - Deletes the thread with the specified ID.
  
- **List Threads:** `GET /api/threads/`
  - Retrieves a list of threads for the authenticated user.

- **Create Message:** `POST /api/message/`
  - Creates a new message in a thread.
  
- **List Messages:** `GET /api/message/<thread_id>/`
  - Retrieves a list of messages for the specified thread.

- **Mark Message as Read:** `PATCH /api/message/<message_id>/read/`
  - Marks the message with the specified ID as read.
  
- **Unread Message Count:** `GET /api/unread-count/`
  - Retrieves the number of unread messages for the authenticated user.

### Authentication:

- Authentication is required to access the API endpoints.
- Use Simple JWT or Django Token for authentication.
- Obtain a token by sending a POST request to the token endpoint (`/api/token/`) with valid credentials.