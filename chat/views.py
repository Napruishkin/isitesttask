from rest_framework import generics, status
from rest_framework.response import Response

from .models import Thread, Message
from .serializers import ThreadSerializer, MessageSerializer

from rest_framework.pagination import LimitOffsetPagination


class CustomPagination(LimitOffsetPagination):
    default_limit = 10
    max_limit = 100


class ThreadCreateAPIView(generics.CreateAPIView):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer

    def create(self, request, *args, **kwargs):
        participants = request.data.get('participants')

        # Check if the number of participants exceeds 2
        if len(participants) > 2:
            return Response({'error': 'A thread can only have 2 participants'}, status=status.HTTP_400_BAD_REQUEST)

        return super().create(request, *args, **kwargs)


class ThreadListAPIView(generics.ListAPIView):
    serializer_class = ThreadSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        user = self.request.user
        return Thread.objects.filter(participants=user)


class ThreadDeleteAPIView(generics.DestroyAPIView):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer


class MessageCreateAPIView(generics.CreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MessageListAPIView(generics.ListAPIView):
    serializer_class = MessageSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        thread_id = self.kwargs['thread_id']
        return Message.objects.filter(thread_id=thread_id)


class MessageReadAPIView(generics.UpdateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class UnreadMessageCountAPIView(generics.RetrieveAPIView):
    def get(self, request, *args, **kwargs):
        user = self.request.user
        unread_count = Message.objects.filter(thread__participants=user, is_read=False).count()
        return Response({'unread_count': unread_count})
