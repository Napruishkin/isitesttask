from django.contrib import admin
from .models import Thread, Message


class MessageInline(admin.TabularInline):
    model = Message
    extra = 0


class ThreadAdmin(admin.ModelAdmin):
    list_display = ('pk', 'created', 'updated')
    search_fields = ('pk',)
    readonly_fields = ('created', 'updated')
    inlines = [MessageInline]

    model = Thread

    def save_related(self, request, form, formsets, change):
        super().save_related(request, form, formsets, change)

        # Fetch the current participants count
        current_participants_count = form.instance.participants.count()

        # If the count exceeds 2, remove the excess participants
        if current_participants_count > 2:
            excess_participants = form.instance.participants.all()[2:]
            form.instance.participants.remove(*excess_participants)
            self.message_user(request, f"Excess participants removed. Thread can't have more than 2 participants.")


class MessageAdmin(admin.ModelAdmin):
    list_display = ('pk', 'sender', 'text', 'thread', 'created', 'is_read')
    search_fields = ('pk', 'text')
    list_filter = ('is_read',)
    readonly_fields = ('created',)
    raw_id_fields = ('thread',)


admin.site.register(Thread, ThreadAdmin)
admin.site.register(Message, MessageAdmin)
