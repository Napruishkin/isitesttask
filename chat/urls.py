from django.urls import path

from .views import ThreadCreateAPIView, ThreadDeleteAPIView, ThreadListAPIView, \
    MessageCreateAPIView, MessageListAPIView, MessageReadAPIView, UnreadMessageCountAPIView

urlpatterns = [
    # Endpoint for creating a new thread
    path('thread/', ThreadCreateAPIView.as_view(), name='thread-create'),

    # Endpoint for deleting a thread with a specific ID
    # Validating that the ID parameter is an integer using <int:pk>
    path('thread/<int:pk>/', ThreadDeleteAPIView.as_view(), name='thread-delete'),

    # Endpoint for retrieving a list of threads for the authenticated user
    path('threads/', ThreadListAPIView.as_view(), name='thread-list'),

    # Endpoint for creating a new message in a thread
    path('message/', MessageCreateAPIView.as_view(), name='message-create'),

    # Endpoint for retrieving a list of messages for a specific thread
    # Validating that the thread ID parameter is an integer using <int:thread_id>
    path('message/<int:thread_id>/', MessageListAPIView.as_view(), name='message-list'),

    # Endpoint for marking a message as read with a specific ID
    # Validating that the message ID parameter is an integer using <int:pk>
    path('message/<int:pk>/read/', MessageReadAPIView.as_view(), name='message-read'),

    # Endpoint for retrieving the number of unread messages for the authenticated user
    path('unread-count/', UnreadMessageCountAPIView.as_view(), name='unread-count'),
]
